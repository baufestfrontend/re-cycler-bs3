$(document).ready(function() {

  //Horizontal collapsible menu
  var sideslider = $('[data-toggle=collapse-side]');
  var sel = sideslider.attr('data-target');
  var sel2 = sideslider.attr('data-target-2');
  sideslider.click(function(event) {
    $(sel).toggleClass('in');
    $(sel2).toggleClass('out');
  });

  // Add margin top to navbar on login scree
  var header = $('header');
  var sidecollapse = $('.side-collapse-container');
  if (header.hasClass('navbar-fixed-top')) {
    sidecollapse.css('margin-top', '60px');
  }

  var home = $('body');
  var categoryShow = $('[data-target=category-section]');
  var categorySection = $('#category-section');

  if (home.hasClass('home')) {
    categorySection.hide();
  }
  categoryShow.bind('click',function(){
    console.log($('this'));
    categorySection.toggle('slow');
  });

  // Login form validation to check if fields are empty
  var loginForm = $('#loginForm');
  var loginSubmit = $('#loginSubmit');

  var dni = $('input[name=dni]');
  var user = $('input[name=username]');
  var password = $('input[name=password]');

  var dniMessage = $('.message.dni');
  var userMessage = $('.message.user');
  var passwordMessage = $('.message.password');

  loginSubmit.bind('click', function() {
    if (dni.val() == '') {
      console.log('Vacio');
      dni.parent().addClass('has-error');
      dniMessage.css('display', 'block');
    } else {
      console.log(dni.val());
      dni.parent().removeClass('has-error');
      dniMessage.css('display', 'none');
    }
    if (user.val() == '') {
      console.log('Vacio');
      user.parent().addClass('has-error');
      userMessage.css('display', 'block');
    } else {
      console.log(user.val());
      user.parent().removeClass('has-error');
      userMessage.css('display', 'none');
    }
    if (password.val() == '') {
      console.log('Vacio');
      password.parent().addClass('has-error');
      passwordMessage.css('display', 'block');
    } else {
      console.log(password.val());
      password.parent().removeClass('has-error');
      passwordMessage.css('display', 'none');
    }
  });

  dni.keyup(function() {
    console.log(dni.parent());
    if (dni.parent().hasClass('has-error')) {
      dni.parent().removeClass('has-error')
      dniMessage.css('display', 'none');
    }
  });

  user.keyup(function() {
    console.log(user.parent());
    if (user.parent().hasClass('has-error')) {
      user.parent().removeClass('has-error')
      userMessage.css('display', 'none');
    }
  });

  password.keyup(function() {
    console.log(password.parent());
    if (password.parent().hasClass('has-error')) {
      password.parent().removeClass('has-error')
      passwordMessage.css('display', 'none');
    }
  });

  // Poorly done slider for category cards

  $(".card.category").mouseover(function() {
    console.log($(this));
    $(this).find('.carousel.slide').carousel({
      interval: 700,
      pause: false
    });
  });

  $(".card.category").mouseleave(function () {
    $(this).find('.carousel.slide').carousel('pause');
  });
});
